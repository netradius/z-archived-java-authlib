<?xml version="1.0" encoding="ISO-8859-1"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.netradius</groupId>
  <artifactId>authlib</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>jar</packaging>
  <name>NetRadius Auth Library</name>
  <description>Provides useful components to aid in authentication and authorization.</description>
  <url>https://bitbucket.org/netradius/java-authlib</url>

  <licenses>
    <license>
      <name>BSD 3-Clause License</name>
      <url>https://opensource.org/licenses/BSD-3-Clause</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <scm>
    <url>https://bitbucket.org/netradius/java-authlib</url>
    <connection>scm:git:git@bitbucket.org:netradius/java-authlib.git</connection>
    <developerConnection>scm:git:git@github.com:netradius/java-authlib.git</developerConnection>
  </scm>

  <properties>
    <!-- Build Settings -->
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <!-- Plugin Versions (Alphabetical Order) -->
    <license-maven-plugin.version>3.0</license-maven-plugin.version>
    <maven-compiler-plugin.version>3.8.1</maven-compiler-plugin.version>
    <maven-gpg-plugin.version>1.6</maven-gpg-plugin.version>
    <maven-javadoc-plugin.version>3.1.1</maven-javadoc-plugin.version>
    <maven-source-plugin.version>3.1.0</maven-source-plugin.version>
    <maven-surefire-plugin.version>2.22.1</maven-surefire-plugin.version>

    <!-- Dependency Versions (Alphabetical Order) -->
    <cryptolib.version>[1.3.7,1.4.0)</cryptolib.version>
    <junit.version>4.12</junit.version>
  </properties>

  <dependencies>
    <dependency>
      <groupId>com.netradius</groupId>
      <artifactId>cryptolib</artifactId>
      <version>${cryptolib.version}</version>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>${junit.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>com.mycila</groupId>
        <artifactId>license-maven-plugin</artifactId>
        <version>${license-maven-plugin.version}</version>
        <configuration>
          <header>LICENSE.txt</header>
          <includes>
            <include>**/*.java</include>
          </includes>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${maven-compiler-plugin.version}</version>
        <configuration>
          <compilerArgs>
            <arg>-Xlint:all</arg>
          </compilerArgs>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-gpg-plugin</artifactId>
        <version>${maven-gpg-plugin.version}</version>
        <executions>
          <execution>
            <id>sign-artifacts</id>
            <phase>verify</phase>
            <goals>
              <goal>sign</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <gpgArguments>
            <arg>--pinentry-mode</arg>
            <arg>loopback</arg>
          </gpgArguments>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>${maven-javadoc-plugin.version}</version>
        <executions>
          <execution>
            <id>attach-javadocs</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>${maven-source-plugin.version}</version>
        <executions>
          <execution>
            <id>attach-sources</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${maven-surefire-plugin.version}</version>
        <configuration>
          <argLine>-Xmx32m</argLine>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <distributionManagement>
    <repository>
      <id>sonatype-releases</id>
      <name>Netradius Release Repository</name>
      <url>https://oss.sonatype.org/service/local/staging/deploy/maven2</url>
    </repository>
    <snapshotRepository>
      <id>sonatype-snapshots</id>
      <name>Netradius Snapshots Repository</name>
      <url>https://oss.sonatype.org/content/repositories/snapshots</url>
    </snapshotRepository>
  </distributionManagement>

  <developers>
    <developer>
      <name>Erik Jensen</name>
      <organization>NetRadius, LLC</organization>
      <organizationUrl>https://www.netradius.com</organizationUrl>
    </developer>
  </developers>

</project>
