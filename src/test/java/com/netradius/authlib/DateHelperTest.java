/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.authlib;

import com.netradius.authlib.helper.DateHelper;
import org.junit.Test;

import java.time.ZonedDateTime;

import static java.lang.System.out;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class DateHelperTest {

  @Test
  public void testFromToISO8601Date() {
    String snow = DateHelper.currentISO8601Date();
    out.println(String.format("Now: %s", snow));
    ZonedDateTime dnow  = DateHelper.fromISO8601Date(snow);
    out.println(String.format("Now: %s", dnow));
    assertEquals(snow, dnow.toString());
  }

  @Test
  public void testFromToBytes() {
    ZonedDateTime zdt = DateHelper.currentDate();
    byte[] bytes = DateHelper.toBytes(zdt);
    ZonedDateTime zdtp = DateHelper.fromBytes(bytes, 0);
    assertThat(zdtp, equalTo(zdt));
  }

  @Test
  public void testFutureDate() {
    String now = DateHelper.currentISO8601Date();
    String future = DateHelper.futureISO8601Date(now, 100);
    ZonedDateTime dnow = DateHelper.fromISO8601Date(now);
    ZonedDateTime dfuture = DateHelper.fromISO8601Date(future);
    assertTrue(dfuture.isAfter(dnow));
  }

  @Test
  public void testWithinTolerance() {
    String snow = DateHelper.currentISO8601Date();
    assertTrue(DateHelper.isISO8601DateInTolerance(snow, 100));
  }

  @Test
  public void testNotWithinTolerance() throws InterruptedException {
    String snow = DateHelper.currentISO8601Date();
    Thread.sleep(101);
    assertFalse(DateHelper.isISO8601DateInTolerance(snow, 100));
  }

  @Test
  public void testExpired() throws InterruptedException {
    String expiration = DateHelper.currentISO8601Date();
    assertFalse(DateHelper.isExpired(expiration, 100));
    Thread.sleep(101);
    assertTrue(DateHelper.isExpired(expiration, 100));
  }
}
