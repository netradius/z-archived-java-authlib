/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.authlib.helper;

import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.commons.lang.ArrayHelper;
import com.netradius.commons.lang.ValidationHelper;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateHelper {

  public static ZonedDateTime fromISO8601Date(String date) {
    ValidationHelper.notBlank(date, "date");
    return ZonedDateTime.parse(date);
  }

  public static String toISO8601Date(ZonedDateTime date) {
    ValidationHelper.notNull(date, "date");
    return date.format(DateTimeFormatter.ISO_INSTANT);
  }

  public static byte[] toBytes(ZonedDateTime date) {
    ValidationHelper.notNull(date, "date");
    Instant instant = date.toInstant();
    long seconds = instant.getEpochSecond();
    byte[] sb = BitTwiddler.ltobe(seconds);
    int nano = instant.getNano();
    byte[] nb = BitTwiddler.itobe(nano);
    return ArrayHelper.concat(sb, nb);
  }

  public static ZonedDateTime fromBytes(byte[] bytes, int offset) {
    ValidationHelper.notEmpty(bytes, "bytes");
    long seconds = BitTwiddler.betol(bytes, offset, offset += 8);
    long nanos = BitTwiddler.betoi(bytes, offset, offset + 4);
    Instant instant = Instant.ofEpochSecond(seconds, nanos);
    return ZonedDateTime.ofInstant(instant, ZoneOffset.UTC);
  }

  public static ZonedDateTime currentDate() {
    return ZonedDateTime.now(ZoneOffset.UTC);
  }

  public static String currentISO8601Date() {
    return toISO8601Date(currentDate());
  }

  public static ZonedDateTime futureDate(ZonedDateTime date, long milliseconds) {
    ValidationHelper.notNull(date, "date");
    ValidationHelper.min(milliseconds, 0, "milliseconds");
    return date.plus(milliseconds, ChronoUnit.MILLIS);
  }

  public static String futureISO8601Date(ZonedDateTime date, long milliseconds) {
    ValidationHelper.notNull(date, "date");
    return toISO8601Date(futureDate(date, milliseconds));
  }

  public static String futureISO8601Date(String date, long milliseconds) {
    ValidationHelper.notBlank(date, "date");
    ZonedDateTime zdt = fromISO8601Date(date);
    return futureISO8601Date(zdt, milliseconds);
  }

  public static boolean isDateInTolerance(ZonedDateTime date, long tolerance) {
    ValidationHelper.notNull(date, "date");
    ValidationHelper.min(tolerance, 0, "tolerance");
    ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
    ZonedDateTime minTimestamp = now.minus(tolerance, ChronoUnit.MILLIS);
    ZonedDateTime maxTimestamp = now.plus(tolerance, ChronoUnit.MILLIS);
    return !date.isBefore(minTimestamp) && !date.isAfter(maxTimestamp);
  }

  public static boolean isISO8601DateInTolerance(String date, long tolerance) {
    ValidationHelper.notBlank(date, "date");
    ValidationHelper.min(tolerance, 0, "tolerance");
    ZonedDateTime parsed = fromISO8601Date(date);
    return isDateInTolerance(parsed, tolerance);
  }

  public static boolean isExpired(ZonedDateTime expiration, long tolerance) {
    ValidationHelper.notNull(expiration, "expiration");
    expiration = expiration.plus(tolerance, ChronoUnit.MILLIS);
    ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
    return now.isAfter(expiration);
  }

  public static boolean isExpired(String expiration, long tolerance) {
    ValidationHelper.notBlank(expiration, "expiration");
    ValidationHelper.min(tolerance, 0, "tolerance");
    ZonedDateTime parsed = fromISO8601Date(expiration);
    return isExpired(parsed, tolerance);
  }
}
