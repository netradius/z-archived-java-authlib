/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.authlib.direct;

import com.netradius.authlib.exception.AuthErrorException;
import com.netradius.authlib.exception.AuthFailedException;
import com.netradius.authlib.exception.AuthParseException;
import com.netradius.authlib.helper.DateHelper;
import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.commons.lang.ArrayHelper;
import com.netradius.commons.lang.StringHelper;
import com.netradius.commons.lang.ValidationHelper;
import com.netradius.cryptolib.CryptoException;
import com.netradius.cryptolib.provider.HMACProvider;

import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;
import java.time.ZonedDateTime;
import java.util.UUID;

import static com.netradius.cryptolib.provider.Provider.SECURE_RANDOM;

public class DirectAuthorization {

  private static final String AUTH_TYPE_ERROR = "V1Authorization cannot handle direct auth type %s";
  private static final String AUTH_SIGN_ERROR = "Error signing message: %s";

  private DirectAuthType type;
  private String keyId;
  private int nonce;
  private ZonedDateTime timestamp;
  private byte[] signature;
  private ZonedDateTime derivedTimestamp;
  private ZonedDateTime derivedExpiration;
  private String audience;
  private String path;

  DirectAuthorization(DirectAuthType type, String keyId, int nonce, ZonedDateTime timestamp, byte[] signature,
                      ZonedDateTime derivedTimestamp, ZonedDateTime derivedExpiration, String audience, String path) {
    ValidationHelper.notNull(type, "type");
    checkType(type);
    ValidationHelper.notNull(timestamp, "timestamp");
    ValidationHelper.notEmpty(signature, "signature");
    ValidationHelper.notBlank(audience, "audience");
    ValidationHelper.notBlank(path, "path");
    this.type = type;
    this.keyId = keyId;
    this.nonce = nonce;
    this.timestamp = timestamp;
    this.signature = signature;
    this.derivedTimestamp = derivedTimestamp;
    this.derivedExpiration = derivedExpiration;
    this.audience = audience;
    this.path = path;
  }

  DirectAuthorization(DirectAuthSecret secret, String audience, String path) {
    ValidationHelper.notNull(secret, "secret");
    checkType(secret.getType());
    this.type = secret.getType();
    this.keyId = secret.getKeyId();
    this.nonce = SECURE_RANDOM.nextInt();
    this.timestamp = DateHelper.currentDate();
    if (secret instanceof DirectAuthDerivedSecret) {
      DirectAuthDerivedSecret derivedSecret = (DirectAuthDerivedSecret)secret;
      this.derivedTimestamp = derivedSecret.getTimestamp();
      this.derivedExpiration = derivedSecret.getExpiration();
    }
    this.audience = audience;
    this.path = path;
    this.signature = sign(secret.getSecret());
  }

  protected String getMessage(String audience, String uri) {
    ValidationHelper.notBlank(audience, "audience");
    ValidationHelper.notBlank(uri, "uri");
    return this.type + "\n"
        + this.keyId + "\n"
        + audience + "\n"
        + uri + "\n"
        + this.timestamp + "\n"
        + this.nonce;
  }

  protected byte[] sign(byte[] secret) {
    ValidationHelper.notEmpty(secret, "secret");
    String message = getMessage(audience, path);
    try {
      return this.type == DirectAuthType.V1_HMAC_SHA512
          ? HMACProvider.sha512().key(secret).hmac().encode(message.getBytes(StandardCharsets.UTF_8))
          : HMACProvider.sha1().key(secret).hmac().encode(message.getBytes(StandardCharsets.UTF_8));
    } catch (CryptoException x) {
      throw new AuthErrorException(x, AUTH_SIGN_ERROR, x.getMessage());
    }
  }

  protected byte[] sign(String secret) {
    ValidationHelper.notBlank(secret, "secret");
    byte[] secretBytes = BitTwiddler.fromb64str(secret);
    return sign(secretBytes);
  }

  public DirectAuthType getType() {
    return type;
  }

  public String getKeyId() {
    return keyId;
  }

  public UUID getDecodedKeyId() {
    return StringHelper.isBlank(this.keyId) ? null : DirectAuthHelper.decodeKeyId(this.keyId);
  }

  public int getNonce() {
    return nonce;
  }

  public void setNonce(int nonce) {
    this.nonce = nonce;
  }

  public ZonedDateTime getTimestamp() {
    return timestamp;
  }

  public String getTimestampISO8601Date() {
    return DateHelper.toISO8601Date(this.timestamp);
  }

  public boolean isTimestampInTolerance(long tolerance) {
    return DateHelper.isDateInTolerance(this.timestamp, tolerance);
  }

  public byte[] getSignature() {
    return signature;
  }

  public String getEncodedSignature() {
    return BitTwiddler.tob64str(this.signature);
  }

  public ZonedDateTime getDerivedTimestamp() {
    return derivedTimestamp;
  }

  public String getDerivedTimestampISO8601Date() {
    return derivedTimestamp == null ? null : DateHelper.toISO8601Date(derivedTimestamp);
  }

  public ZonedDateTime getDerivedExpiration() {
    return derivedExpiration;
  }

  public String getDerivedExpirationISO8601Date() {
    return derivedExpiration == null ? null : DateHelper.toISO8601Date(derivedExpiration);
  }

  public boolean isDerivedExpired(long tolerance) {
    return DateHelper.isExpired(this.derivedExpiration, tolerance);
  }

  public boolean isDerived() {
    return derivedTimestamp != null && derivedExpiration != null;
  }

  public String getHeaderValue() {
    String signature = BitTwiddler.tob64str(this.signature);
    String timestamp = getTimestampISO8601Date();
    String value = this.type.toString() + " " +
        this.keyId + " " +
        this.nonce + " " +
        timestamp + " " +
        signature + " ";
    if (isDerived()) {
      String derivedTimestamp = getDerivedTimestampISO8601Date();
      String derivedExpiration = getDerivedTimestampISO8601Date();
      return value + derivedTimestamp + " " + derivedExpiration;
    }
    return value;
  }

  public void authorize(DirectAuthSecret secret, long timestampTolerance, long derivedKeyTolerance) {
    ValidationHelper.notNull(secret, "secret");
    ValidationHelper.min(timestampTolerance, 0, "timestampTolerance");
    ValidationHelper.min(derivedKeyTolerance, 0, "derivedKeyTolerance");

    if (!isTimestampInTolerance(timestampTolerance)) {
      throw new AuthFailedException("Authorization timestamp is not within acceptable expiration tolerances");
    }

    if (isDerived() && isDerivedExpired(derivedKeyTolerance)) {
      throw new AuthFailedException("Derived key is not within acceptable expiration tolerances");
    }

    if (isDerived()) {
      secret = secret.derive(derivedTimestamp, derivedExpiration, audience);
    }

    byte[] csign = sign(secret.getSecret());
    if (!ArrayHelper.equals(csign, this.signature)) {
      throw new AuthFailedException("Signatures do not match");
    }
  }

  private static void checkType(DirectAuthType type) {
    if (type != DirectAuthType.V1_HMAC_SHA1 && type != DirectAuthType.V1_HMAC_SHA512) {
      throw new IllegalArgumentException(String.format(AUTH_TYPE_ERROR, type));
    }
  }

  private static DirectAuthType parseType(String type) {
    try {
      ValidationHelper.notBlank(type, "type");
      DirectAuthType dat = DirectAuthType.fromString(type);
      checkType(dat);
      return dat;
    } catch (IllegalArgumentException x) {
      throw new AuthParseException(AUTH_TYPE_ERROR, type);
    }
  }

  private static int parseNonce(String str) {
    try {
      ValidationHelper.notBlank(str, "str");
      return Integer.parseInt(str);
    } catch (IllegalArgumentException x) {
      throw new AuthParseException("Authorization contains invalid nonce [%s]", str);
    }
  }

  private static String parseKeyId(String keyId) {
    try {
      DirectAuthHelper.decodeKeyId(keyId);
      return keyId;
    } catch (IllegalArgumentException x) {
      throw new AuthParseException("Authorization contains invalid key ID [%s]", keyId);
    }
  }

  private static ZonedDateTime parseZoneDateTime(String str) {
    try {
      return DateHelper.fromISO8601Date(str);
    } catch (IllegalArgumentException | DateTimeException x) {
      throw new AuthParseException("Authorization contains invalid date [%s]", str);
    }
  }

  private static byte[] parseSignature(String str) {
    try {
      ValidationHelper.notBlank(str, "str");
      return BitTwiddler.fromb64str(str);
    } catch (IllegalArgumentException x) {
      throw new AuthParseException("Authorization contains invalid signature [%s]", str);
    }
  }

  private boolean derivedEquals(DirectAuthorization auth) {
    return !isDerived() ||
        (derivedTimestamp.equals(auth.derivedTimestamp) && derivedExpiration.equals(auth.derivedExpiration));
  }

  @Override
  public int hashCode() {
    return type.hashCode() ^ keyId.hashCode() ^ nonce ^ timestamp.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof DirectAuthorization) {
      DirectAuthorization auth = (DirectAuthorization)obj;
      return type == auth.getType() && keyId.equals(auth.keyId) && nonce == auth.nonce
          && timestamp.equals(auth.timestamp) && ArrayHelper.equals(signature, auth.signature)
          && derivedEquals(auth);
    }
    return false;
  }

  public static DirectAuthorization fromHeaderValue(String headerValue, String audience, String path) {
    ValidationHelper.notBlank(headerValue, "headerValue");
    ValidationHelper.notBlank(audience, "audience");
    ValidationHelper.notBlank(path, "path");

    String[] parts = headerValue.split(" ");
    if (parts.length != 5 && parts.length != 7) {
      throw new AuthParseException("Expected authorization to contain 5 or 7 parts, received %d parts", parts.length);
    }

    DirectAuthType type = parseType(parts[0]);
    String keyId = parseKeyId(parts[1]);
    int nonce = parseNonce(parts[2]);
    ZonedDateTime timestamp = parseZoneDateTime(parts[3]);
    byte[] signature = parseSignature(parts[4]);
    ZonedDateTime derivedTimestamp = parts.length == 7 ? parseZoneDateTime(parts[5]) : null;
    ZonedDateTime derivedExpiration = parts.length == 7 ? parseZoneDateTime(parts[6]) : null;

    return new DirectAuthorization(type, keyId, nonce, timestamp, signature,
        derivedTimestamp, derivedExpiration, audience, path);
  }
}
