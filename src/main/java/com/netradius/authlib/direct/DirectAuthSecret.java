/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.authlib.direct;

import com.netradius.authlib.exception.AuthErrorException;
import com.netradius.authlib.helper.DateHelper;
import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.commons.lang.ArrayHelper;
import com.netradius.commons.lang.ValidationHelper;
import com.netradius.commons.uuid.UUIDHelper;
import com.netradius.cryptolib.CryptoException;
import com.netradius.cryptolib.provider.HMACProvider;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.UUID;

public class DirectAuthSecret {

  public static final long DURATION_MINUTE = 1000 * 60;
  public static final long DURATION_HOUR = DURATION_MINUTE * 60;
  public static final long DURATION_DAY = DURATION_HOUR * 24;
  public static final long DURATION_WEEK = DURATION_DAY * 7;
  public static final DirectAuthType DEFAULT_AUTH_TYPE = DirectAuthType.V1_HMAC_SHA512;
  public static final int SECRET_LENGTH_BYTES = 64;

  private DirectAuthType type;
  private String keyId;
  private byte[] secret;

  public DirectAuthSecret(DirectAuthType type, String keyId, byte[] secret) {
    ValidationHelper.notNull(type, "type");
    ValidationHelper.notNull(keyId, "keyId");
    ValidationHelper.notNull(secret, "secret");
    if (secret.length != 64) {
      throw new IllegalArgumentException(
          String.format("Expected key length of 64 bytes and received %d bytes", secret.length));
    }
    this.type = type;
    this.keyId = keyId;
    this.secret = secret;
  }

  public DirectAuthType getType() {
    return type;
  }

  public String getKeyId() {
    return keyId;
  }

  public UUID getDecodedKeyId() {
    return DirectAuthHelper.decodeKeyId(keyId);
  }

  public byte[] getSecret() {
    return secret;
  }

  public String getEncodedSecret() {
    return BitTwiddler.tob64str(secret);
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(secret);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof DirectAuthSecret) {
      DirectAuthSecret directAuthSecret = (DirectAuthSecret)obj;
      return type == directAuthSecret.getType() && keyId.equals(directAuthSecret.keyId)
          && ArrayHelper.equals(secret, directAuthSecret.secret);
    }
    return false;
  }

  @Override
  public String toString() {
    return keyId;
  }

  protected byte[] serializeType() {
    int id = type.id();
    return BitTwiddler.itobe(id);
  }

  protected byte[] serializeKeyId() {
    UUID uuid = DirectAuthHelper.decodeKeyId(keyId);
    return UUIDHelper.toBytes(uuid);
  }

  public byte[] serializeBytes() {
    byte[] tb = serializeType();
    byte[] kb = serializeKeyId();
    return ArrayHelper.concat(tb, kb, secret);
  }

  public String serialize() {
    byte[] bytes = serializeBytes();
    return BitTwiddler.tob64str(bytes);
  }

  public DirectAuthorization authorization(String audience, String path) {
    ValidationHelper.notBlank(audience, "audience");
    ValidationHelper.notBlank(path, "path");
    return new DirectAuthorization(this, audience, path);
  }

  public DirectAuthDerivedSecret derive(long expiration, String audience) {
    ValidationHelper.min(expiration, 0, "expiration");
    ZonedDateTime zdt = DateHelper.futureDate(DateHelper.currentDate(), expiration);
    return derive(zdt, audience);
  }

  public DirectAuthDerivedSecret derive(String expiration, String audience) {
    ValidationHelper.notBlank(expiration, "expiration");
    ZonedDateTime zdt = DateHelper.fromISO8601Date(expiration);
    return derive(zdt, audience);
  }

  public DirectAuthDerivedSecret derive(ZonedDateTime expiration, String audience) {
    ZonedDateTime timestamp = DateHelper.currentDate();
    return derive(timestamp, expiration, audience);
  }

  public DirectAuthDerivedSecret derive(ZonedDateTime timestamp, ZonedDateTime expiration, String audience) {
    ValidationHelper.notNull(expiration, "expiration");
    ValidationHelper.notBlank(audience, "audience");
    HMACProvider.HMACBuilder builder;
    switch (type) {
      case V1_HMAC_SHA1:
        builder = HMACProvider.sha1();
        break;
      case V1_HMAC_SHA512:
        builder = HMACProvider.sha512();
        break;
      default:
        throw new AuthErrorException("Unknown auth type: %s", type);
    }
    try {
      String timestampStr = DateHelper.toISO8601Date(timestamp);
      String expirationStr = DateHelper.toISO8601Date(expiration);
      byte[] dkey = builder.key(secret).hmac().encode(type.toString().getBytes(StandardCharsets.UTF_8));
      dkey = builder.key(dkey).hmac().encode(audience.getBytes(StandardCharsets.UTF_8));
      dkey = builder.key(dkey).hmac().encode(timestampStr.getBytes(StandardCharsets.UTF_8));
      dkey = builder.key(dkey).hmac().encode(expirationStr.getBytes(StandardCharsets.UTF_8));
      return new DirectAuthDerivedSecret(type, keyId, dkey, timestamp, expiration, audience);
    } catch (CryptoException x) {
      throw new AuthErrorException(x, "Error deriving key: %s", x.getMessage());
    }
  }


  static DirectAuthType deserializeType(byte[] bytes, int offset) {
    int id = BitTwiddler.betoi(bytes, offset, offset + 4);
    return DirectAuthType.fromId(id);
  }

  static String deserializeKeyId(byte[] bytes, int offset) {
    UUID uuid = UUIDHelper.toUUID(bytes, offset);
    return DirectAuthHelper.encodeKeyId(uuid);
  }

  static byte[] deserializeSecret(byte[] bytes, int offset) {
    return Arrays.copyOfRange(bytes, offset, offset + SECRET_LENGTH_BYTES);
  }

  static DirectAuthSecret deserialize(byte[] bytes) {
    int offset = 0;
    DirectAuthType type = deserializeType(bytes, offset);
    String keyId = deserializeKeyId(bytes, offset += 4);
    byte[] secret = deserializeSecret(bytes, offset + 16);
    return new DirectAuthSecret(type, keyId, secret);
  }

  public static DirectAuthSecret generate(DirectAuthType type) {
    String keyId = DirectAuthHelper.generateKeyId();
    byte[] secret = DirectAuthHelper.generateSecret(type);
    return new DirectAuthSecret(type, keyId, secret);
  }

  public static DirectAuthSecret generate() {
    return generate(DirectAuthType.V1_HMAC_SHA512);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {

    protected DirectAuthType type;
    protected byte[] serialized;
    protected String keyId;
    protected byte[] secretKey;

    protected Builder() {}

    public Builder serialized(byte[] serialized) {
      this.serialized = serialized;
      return this;
    }

    public Builder serialized(String serialized) {
      this.serialized = BitTwiddler.fromb64str(serialized);
      return this;
    }

    public Builder keyId(String keyId) {
      this.keyId = keyId;
      return this;
    }

    public Builder keyId(UUID keyId) {
      this.keyId = DirectAuthHelper.encodeKeyId(keyId);
      return this;
    }

    public Builder secretKey(byte[] secretKey) {
      this.secretKey = secretKey;
      return this;
    }

    public Builder secretKey(String secretKey) {
      this.secretKey = BitTwiddler.fromb64str(secretKey);
      return this;
    }

    public Builder type(DirectAuthType type) {
      this.type = type;
      return this;
    }

    public Builder type(String type) {
      this.type = DirectAuthType.fromString(type);
      return this;
    }

    public DirectAuthSecret build() {
      if (serialized != null) {
        return deserialize(serialized);
      }
      if (type == null) {
        type = DEFAULT_AUTH_TYPE;
      }
      ValidationHelper.notBlank(keyId, "keyId");
      ValidationHelper.notEmpty(secretKey, "secretKey");
      return new DirectAuthSecret(type, keyId, secretKey);
    }
  }

}
