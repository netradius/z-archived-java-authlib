/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.authlib.direct;

import com.netradius.authlib.helper.DateHelper;
import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.commons.lang.ArrayHelper;
import com.netradius.commons.lang.ValidationHelper;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Arrays;

public class DirectAuthDerivedSecret extends DirectAuthSecret {

  private ZonedDateTime timestamp;
  private ZonedDateTime expiration;
  private String audience;

  public DirectAuthDerivedSecret(
      DirectAuthType type, String keyId, byte[] secret, ZonedDateTime timestamp,
      ZonedDateTime expiration, String audience) {
    super(type, keyId, secret);
    ValidationHelper.notNull(timestamp, "timestamp");
    ValidationHelper.notNull(expiration, "expiration");
    ValidationHelper.notBlank(audience, "audience");
    this.timestamp = timestamp;
    this.expiration = expiration;
    this.audience = audience;
  }

  public ZonedDateTime getTimestamp() {
    return timestamp;
  }

  public String getTimestampISO8601Date() {
    return DateHelper.toISO8601Date(this.timestamp);
  }

  public ZonedDateTime getExpiration() {
    return expiration;
  }

  public String getExpirationISO8601Date() {
    return DateHelper.toISO8601Date(this.expiration);
  }

  public boolean isExpired(long tolerance) {
    return DateHelper.isExpired(this.expiration, tolerance);
  }

  public String getAudience() {
    return audience;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof DirectAuthDerivedSecret) {
      DirectAuthDerivedSecret derivedSecret = (DirectAuthDerivedSecret)obj;
      return getType() == derivedSecret.getType() && getKeyId().equals(derivedSecret.getKeyId())
          && ArrayHelper.equals(getSecret(), derivedSecret.getSecret())
          && timestamp.equals(derivedSecret.timestamp)
          && expiration.equals(derivedSecret.expiration)
          && audience.equals(derivedSecret.audience);
    }
    return false;
  }

  @Override
  public byte[] serializeBytes() {
    byte[] tb = serializeType(); // 4 bytes
    byte[] kb = serializeKeyId(); // 16 bytes
    byte[] timestampBytes = DateHelper.toBytes(timestamp); // 12 bytes
    byte[] expirationBytes = DateHelper.toBytes(expiration); // 12 bytes
    byte[] audienceBytes = audience.getBytes(StandardCharsets.UTF_8);
    return ArrayHelper.concat(tb, kb, timestampBytes, expirationBytes, getSecret(), audienceBytes);
  }

  @Override
  public String serialize() {
    return BitTwiddler.tob64str(this.serializeBytes());
  }

  protected static String deserializeAudience(byte[] bytes, int offset) {
    bytes = Arrays.copyOfRange(bytes, offset + 64, bytes.length);
    return new String(bytes, StandardCharsets.UTF_8);
  }

  static DirectAuthDerivedSecret deserialize(byte[] bytes) {
    int offset = 0;
    DirectAuthType type = deserializeType(bytes, offset);
    String keyId = deserializeKeyId(bytes, offset += 4);
    ZonedDateTime timestamp = DateHelper.fromBytes(bytes, offset += 16);
    ZonedDateTime expiration = DateHelper.fromBytes(bytes, offset += 12);
    byte[] secret = deserializeSecret(bytes, offset += 12); // 64 bytes
    String audience = deserializeAudience(bytes, offset);
    return new DirectAuthDerivedSecret(type, keyId, secret, timestamp, expiration, audience);
  }

  static DirectAuthDerivedSecret deserialize(String str) {
    byte[] bytes = BitTwiddler.fromb64str(str);
    return DirectAuthDerivedSecret.deserialize(bytes);
  }
}
