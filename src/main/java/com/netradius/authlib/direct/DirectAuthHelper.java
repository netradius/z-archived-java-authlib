/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.authlib.direct;

import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.commons.lang.ValidationHelper;
import com.netradius.commons.uuid.UUIDHelper;
import com.netradius.cryptolib.CryptoException;
import com.netradius.cryptolib.provider.HMACProvider;

import java.util.UUID;

class DirectAuthHelper {

  static String encodeKeyId(UUID keyId) {
    ValidationHelper.notNull(keyId, "keyId");
    return BitTwiddler.tob64str(UUIDHelper.toBytes(keyId), BitTwiddler.URL_MODIFIED_BASE64_CHARS);
  }

  static UUID decodeKeyId(String keyId) {
    ValidationHelper.notBlank(keyId, "keyId");
    return UUIDHelper.toUUID(BitTwiddler.fromb64str(keyId, BitTwiddler.URL_MODIFIED_BASE64_CHARS));
  }

  static String generateKeyId() {
    UUID uuid = UUID.randomUUID();
    return encodeKeyId(uuid);
  }

  static byte[] generateSecret(DirectAuthType type) {
    try {
      switch (type) {
        case V1_HMAC_SHA1:
          return HMACProvider.sha1().hmac().keyBytes();
        case V1_HMAC_SHA512:
          return HMACProvider.sha512().hmac().keyBytes();
        default:
          throw new IllegalArgumentException(String.format("Unknown type %s", type.toString()));
      }
    } catch (CryptoException x) {
      throw new IllegalStateException("Error generating secret: " + x.getMessage(), x);
    }
  }
}
